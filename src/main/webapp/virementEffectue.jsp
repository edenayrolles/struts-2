<%@include file="header.jsp"%>

<div class="callout large primary">
    <div class="row column text-center">
        <h1>Virement ok :</h1>
        <div>
            <ul>
                <li>Compte Source : <s:property value="virement.compteSource.libelle" /> </li>
                <li>Compte Destination : <s:property value="virement.compteSource.libelle" /> </li>
                <li>Montant : <s:property value="virement.montant" /></li>
            </ul>
        </div>
    </div>
</div>