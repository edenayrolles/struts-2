<%@include file="header.jsp"%>

<div class="callout large primary">
    <div class="row column text-center">
        <h1>Detail du compte : <s:property value="compteEpargne.libelle"/> </h1>
        <ul>
            <li>Solde : <s:property value="compteEpargne.solde"/></li>
            <li>Plafond : <s:property value="compteEpargne.plafond"/></li>
            <li>Taux d'interet : <s:property value="compteEpargne.tauxInteret"/></li>
        </ul>
    </div>
</div>