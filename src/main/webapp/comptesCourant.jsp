<%@include file="header.jsp"%>


<div class="callout large primary">
    <table>
        <thead>
        <tr>
            <th width="25">Id</th>
            <th width="200">Libelle</th>
            <th width="150">Solde</th>
            <th>Montant de d�couvert autoris�</th>
        </tr>
        </thead>
        <tbody>

        <s:iterator value="#session.client.compteCourantList" id="c">

            <tr class="<s:if test="#c.solde<0">alert callout</s:if>">
                <td><s:property value="#c.id" /></td>
                <td><s:property value="#c.libelle" /></td>
                <td><s:property value="#c.solde" /></td>
                <td><s:property value="#c.decouvertAutorise" /></td>

            </tr>
        </s:iterator>


        </tbody>
    </table>
</div>