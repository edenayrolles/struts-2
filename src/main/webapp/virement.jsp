<%@include file="header.jsp"%>

<div class="callout large primary">
    <div class="row column text-center">
        <h1>Virement</h1>
        <div>
            <s:if test="hasActionErrors()">
                <div class="errors">
                    <s:actionerror/>
                </div>
            </s:if>
            <s:form action="Virement">
                <s:select label="Compte Source"
                          name="cptSrc"
                          list="#session.client.comptes"
                          listValue="libelle + ' : ' + solde"
                          listKey="id"/>
                <s:select label="Compte Destination"
                          name="cptDest"
                          list="#session.client.comptes"
                          listValue="libelle + ' : ' + solde"
                          listKey="id"/>
                <s:textfield label="montant" name="montant"/>
                <s:submit label="Transferer" />
            </s:form>
        </div>
    </div>
</div>