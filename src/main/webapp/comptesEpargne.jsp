<%@include file="header.jsp"%>


<div class="callout large primary">
    <table>
        <thead>
        <tr>
            <th width="25">Id</th>
            <th width="200">Libelle</th>
            <th width="150">Solde</th>
            <th>Taux Interet</th>
            <th>Plafond</th>
        </tr>
        </thead>
        <tbody>

        <s:iterator value="#session.client.compteEpargneList" id="c">

            <s:url action="DetailCompteEpargne" id="url">
                <s:param name="compteId" value="#c.id"></s:param>
            </s:url>

            <tr class="<s:if test="#c.solde<0">alert callout</s:if>">
                <td><s:property value="#c.id" /></td>
                <td>
                    <a href="<s:property value="#url"/>">
                        <s:property value="#c.libelle" />
                    </a>
                </td>
                <td><s:property value="#c.solde" /></td>
                <td><s:property value="#c.tauxInteret" /></td>
                <td><s:property value="#c.plafond" /></td>

            </tr>
        </s:iterator>


        </tbody>
    </table>
</div>