<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/app.css" />
</head>
<script src="js/vendor/jquery.min.js"></script>
<script src="js/vendor/what-input.min.js"></script>
<script src="js/foundation.min.js"></script>
<script src="js/app.js"></script>

<div class="top-bar">
    <div class="top-bar-left">
        <ul class="menu">
            <li class="menu-text">J//Academie</li>
            <li class="menu-text">Welcome : <s:property value="#session.client.prenom + ' ' + #session.client.nom"/></li>
        </ul>
    </div>
    <div class="top-bar-right">
        <ul class="menu">
            <li>
                <a href="<s:url action='ListCompteCourant' />">
                    Liste des comptes Courant
                </a>
            </li>
            <li>
                <a href="<s:url action='ListCompteEpargne' />">
                    Liste des comptes Epargne
                </a>
            </li>
            <li>
                <a href="<s:url action='Virement' />">
                    Virement
                </a>
            </li>
        </ul>
    </div>
</div>
