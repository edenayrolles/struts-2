/* Created on 23 juin 2005 */
package com.bankonet.service;

import java.util.ArrayList;

import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;
import com.bankonet.model.Compte;
import com.bankonet.model.CompteCourant;
import com.bankonet.model.CompteEpargne;
import com.bankonet.model.Virement;

/**
 * @author amalbert
 */
class BanqueServiceBouchon implements BanqueService {

	public Client findClient(String login, String motDePasse) throws BankonetException {
		Client c = new Client(10, "Bouchon", "John");
		c.setLogin(login);

		ArrayList<Compte> courantList = new ArrayList<Compte>();
		courantList.add(new CompteCourant(12, "Compte courant bouchon", 200, 1000));
		c.getComptes().addAll(courantList);

		ArrayList<Compte> epargneList = new ArrayList<Compte>();
		epargneList.add(new CompteEpargne(13, "Compte epargne bouchon", 1000, 0.5f, 500));
		c.getComptes().addAll(epargneList);
		return c;
	}

	public Virement effectuerVirement(Compte cptSource, Compte cptDest, float montant) throws BankonetException {
		if (cptSource.debitAutorise(montant) && (cptDest.creditAutorise(montant))) {
			cptDest.setSolde(cptDest.getSolde() + montant);
			cptSource.setSolde(cptSource.getSolde() - montant);
		}

		return new Virement(cptSource, cptDest, montant);
	}

}