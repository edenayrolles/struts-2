package com.bankonet.service;

import java.util.List;

import com.bankonet.dao.ClientDAO;
import com.bankonet.dao.CompteDAO;
import com.bankonet.dao.jdbc.ClientDaoJdbc;
import com.bankonet.dao.jdbc.CompteDaoJdbc;
import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;
import com.bankonet.model.Compte;
import com.bankonet.model.CompteCourant;
import com.bankonet.model.CompteEpargne;
import com.bankonet.model.Virement;

/**
 * @author amalbert
 */
public class BanqueServiceClassique implements BanqueService {
	CompteDAO compteDAO = new CompteDaoJdbc();

	ClientDAO clientDAO = new ClientDaoJdbc();

	public Client findClient(String login, String motDePasse) throws BankonetException {
        System.out.println("findClient(String login, String motDePasse)" + login + motDePasse );
        Client client = clientDAO.findClient(login, motDePasse);
		loadComptes(client);
		return client;
	}

	private void loadComptes(Client client) throws BankonetException {
		List<Compte> comptes = compteDAO.findComptesByClient(client);
		for (Compte c : comptes) {
			if (c instanceof CompteCourant)
				client.getCompteCourantList().add((CompteCourant) c);
			else
				client.getCompteEpargneList().add((CompteEpargne) c);
		}
	}

	public Virement effectuerVirement(Compte cptSource, Compte cptDest, float montant) throws BankonetException {
		if (cptSource.getId() == cptDest.getId()) {
			throw new BankonetException("Les deux comptes doivent etre differents");
		}

		if (cptSource.debitAutorise(montant) && (cptDest.creditAutorise(montant))) {
			compteDAO.transfertMontant(cptSource, cptDest, montant);
		}

		return new Virement(cptSource, cptDest, montant);
	}

}