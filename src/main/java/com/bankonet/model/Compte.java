package com.bankonet.model;

import java.io.Serializable;

/**
 * @author amalbert
 */
public abstract class Compte implements Serializable {
	private static final long serialVersionUID = -5196022042300125595L;

	protected int id;
	protected String libelle;
	protected float solde;

	public Compte() {
		super();
	}

	public Compte(int id, String libelle, float solde) {
		this.id = id;
		this.libelle = libelle;
		this.solde = solde;

	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public boolean creditAutorise(float montant) throws BankonetException {
		return false;
	}

	public boolean debitAutorise(float montant) throws BankonetException {
		return false;
	}

	public String getLibelle() {
		return libelle;
	}

	public int getId() {
		return id;
	}

	public String toString() {
		return id + " " + libelle + " " + solde;
	}

	public Float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public void setId(int id) {
		this.id = id;
	}
}
