package com.bankonet.model;

public class VirementBankonetException extends Exception {
	private static final long serialVersionUID = -6758821202324428297L;

	public VirementBankonetException() {
		super();
	}

	public VirementBankonetException(String message, Throwable cause) {
		super(message, cause);
	}

	public VirementBankonetException(String message) {
		super(message);
	}

	public VirementBankonetException(Throwable cause) {
		super(cause);
	}

}
