package com.bankonet.model;

/**
 * @author Sysdeo
 */
public class CompteEpargne extends Compte {
	private static final long serialVersionUID = 8978567552414064480L;

	private float tauxInteret;
	private float plafond;

	public CompteEpargne(int id, String libelle, float solde, float tauxInteret, float plafond) {
		super(id, libelle, solde);
		this.tauxInteret = tauxInteret;
		this.plafond = plafond;
	}

	/**
	 * Le montant ne doit pas etre superieur au plafond de credit autorise en
	 * une fois
	 */
	public boolean creditAutorise(float montant) throws BankonetException {
		if (montant < plafond) {
			return true;
		} else {
			throw new BankonetException("Le compte epargne " + id + " a pour plafond de credit : " + plafond);
		}
	}

	public boolean debitAutorise(float montant) throws BankonetException {
		if (solde - montant >= 0) {
			return true;
		} else {
			throw new BankonetException("Montant trop eleve : le solde du compte epargne " + id + " ne peut etre negatif");
		}
	}

	public float getPlafond() {
		return plafond;
	}

	public void setPlafond(float newPlafond) {
		plafond = newPlafond;
	}

	public float getTauxInteret() {
		return tauxInteret;
	}

	public void setTauxInteret(float newTauxInteret) {
		tauxInteret = newTauxInteret;
	}
}