package com.bankonet.model;

/**
 * Classe utilisee pour lancer les exceptions liees � la partie "metier" de
 * l'application Bankonet.
 * 
 * @author amalbert
 */
public class BankonetException extends Exception {
	public BankonetException(Throwable cause) {
		super(cause);
	}

	private static final long serialVersionUID = 8179813305563241451L;

	public BankonetException() {
		super();
	}

	public BankonetException(String message) {
		super(message);
	}

}
