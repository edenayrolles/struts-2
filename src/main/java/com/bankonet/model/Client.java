package com.bankonet.model;

import java.util.ArrayList;
import java.util.List;

public class Client {
	private int id;
	private String nom;
	private String prenom;
	private String login;
	private String password;

	private List<CompteCourant> compteCourantList;
	private List<CompteEpargne> compteEpargneList;

	public Client() {
		this.compteCourantList = new ArrayList<CompteCourant>();
		this.compteEpargneList = new ArrayList<CompteEpargne>();
	}

	public Client(int id, String nom, String prenom) {
		this();
		this.nom = nom;
		this.prenom = prenom;
		this.id = id;
	}
	
	public String getLogin() {
		return login;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Compte getCompte(int cptId) {
		List<Compte> comptes = getComptes();
		for (Compte compte : comptes) {
			if (compte.getId() == cptId)
				return compte;
		}

		return null;
	}

	public List<Compte> getComptes() {
		List<Compte> comptes = new ArrayList<Compte>();

		comptes.addAll(compteEpargneList);
		comptes.addAll(compteCourantList);

		return comptes;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<CompteCourant> getCompteCourantList() {
		return compteCourantList;
	}

	public void setCompteCourantList(List<CompteCourant> compteCourantList) {
		this.compteCourantList = compteCourantList;
	}

	public List<CompteEpargne> getCompteEpargneList() {
		return compteEpargneList;
	}

	public void setCompteEpargneList(List<CompteEpargne> compteEpargneList) {
		this.compteEpargneList = compteEpargneList;
	}

	public void setId(int id) {
		this.id = id;
	}
}
