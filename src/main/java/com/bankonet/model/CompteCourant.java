package com.bankonet.model;

/**
 * @author amalbert
 */
public class CompteCourant extends Compte {
	private static final long serialVersionUID = -4527254816456866994L;

	private float decouvertAutorise;

	public CompteCourant(int id, String libelle, float solde, float decouvertAutorise) {

		super(id, libelle, solde);

		this.decouvertAutorise = decouvertAutorise;
	}

	/**
	 * Le credit d'un compte courant est toujours autorise
	 */
	public boolean creditAutorise(float montant) throws BankonetException {
		return true;
	}

	public boolean debitAutorise(float montant) throws BankonetException {
		if (solde + decouvertAutorise >= montant) {
			return true;
		}
		throw new BankonetException("Montant trop eleve : le solde du compte courant " + id + " ne peut descendre en dessous du decouvert autorise");
	}

	public float getDecouvertAutorise() {
		return decouvertAutorise;
	}
}