package com.bankonet.action;

import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;
import com.bankonet.model.Compte;
import com.bankonet.model.Virement;
import com.bankonet.service.BanqueServiceManager;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

/**
 * Created by edenayrolles on 04/12/2015.
 */
public class VirementAction extends ActionSupport implements SessionAware {

    private Map<String, Object> session = null;
    private int cptSrc;
    private int cptDest;
    private float montant;
    private Virement virement;


    public String execute() {

        Client client = (Client) session.get("client");

            try {
                if (cptSrc > 0 && cptDest > 0) {
                    virement = BanqueServiceManager.getBanqueService()
                        .effectuerVirement(client.getCompte(cptSrc), client.getCompte(cptDest), montant);

                    return SUCCESS;
                } else {
                    addActionError("Il faut un compte source et un compte destinataire");
                    //return INPUT;
                }
            } catch (BankonetException e) {
                //e.printStackTrace();
                //return INPUT;
                addActionError("Une erreure est survenue lors du transfert, try again  !!");

            }

        return INPUT;

    }


    public void setCptSrc(int cptSrc) {
        this.cptSrc = cptSrc;
    }

    public int getCptSrc() {
        return cptSrc;
    }

    public void setCptDest(int cptDest) {
        this.cptDest = cptDest;
    }

    public int getCptDest() {
        return cptDest;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }

    public float getMontant() {
        return montant;
    }

    public Virement getVirement() {
        return virement;
    }

    public void setVirement(Virement virement) {
        this.virement = virement;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.session = map;
    }
}
