package com.bankonet.action;

import com.bankonet.model.Client;
import com.bankonet.model.CompteEpargne;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

/**
 * Created by edenayrolles on 04/12/2015.
 */
public class DetailCompteEpargneAction extends ActionSupport implements SessionAware {

    private int compteId;

    private CompteEpargne compteEpargne;

    private Map<String, Object> session;

    public String execute() {

        Client client = (Client)session.get("client");
        this.compteEpargne = (CompteEpargne)client.getCompte(compteId);

        return SUCCESS;
    }

    public void setCompteId(int compteId) {
        this.compteId = compteId;
    }

    public int getCompteId() {
        return compteId;
    }

    public CompteEpargne getCompteEpargne() {
        return compteEpargne;
    }

    public void setCompteEpargne(CompteEpargne compteEpargne) {
        this.compteEpargne = compteEpargne;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        session = map;
    }
}
