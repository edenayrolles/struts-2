package com.bankonet.action;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;
import com.bankonet.service.BanqueService;
import com.bankonet.service.BanqueServiceManager;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport implements SessionAware {

	private Map<String, Object> session = null;


    private Client client = new Client();


	public String execute() {
		BanqueService service = BanqueServiceManager.getBanqueService();

		try {
            this.client = service.findClient(this.client.getLogin(), this.client.getPassword());
			session.put("client", this.client);
            return SUCCESS;
		} catch (Exception e) {
			//addActionError("Mauvais login/ password");
            return INPUT;
		}
	}


    @Override
    public void setSession(Map<String, Object> map) {
        this.session = map;
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
