/*
 * Created on 13 mars 2006
 */
package com.bankonet.dao;

import java.sql.Connection;

import com.bankonet.model.BankonetException;

public class AbstractDAO {
	protected void closeConnection(Connection connection) throws BankonetException {
		try {
			if (connection != null && (!connection.isClosed())) {
				connection.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankonetException("Erreur pendant la fermeture de la connection");
		}
	}
}
