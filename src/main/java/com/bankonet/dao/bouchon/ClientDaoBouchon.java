package com.bankonet.dao.bouchon;

import java.util.HashMap;
import java.util.Map;

import com.bankonet.dao.ClientDAO;
import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;

/**
 * @author amalbert
 */
public class ClientDaoBouchon implements ClientDAO {

	private Map<String, Client> clientMap;

	public ClientDaoBouchon() {
		clientMap = new HashMap<String, Client>();
		clientMap.put("david", new Client(1, "Dupond", "david"));
		clientMap.put("sandie", new Client(2, "Durand", "sandie"));
		clientMap.put("fabrice", new Client(3, "Martin", "fabrice"));
	}

	public Client findClient(String login, String motDePasse) throws BankonetException {
		if (login.equals("david") && motDePasse.equals("david")) {
			return clientMap.get("david");
		} else if (login.equals("sandie") && motDePasse.equals("sandie")) {
			return clientMap.get("sandie");
		} else if (login.equals("fabrice") && motDePasse.equals("fabrice")) {
			return clientMap.get("fabrice");
		} else {
			throw new BankonetException("Client non trouve : login ou mot de passe invalide");
		}
	}

	public void updateClient(Client client) throws BankonetException {
		clientMap.put(client.getNom(), client);
	}

}