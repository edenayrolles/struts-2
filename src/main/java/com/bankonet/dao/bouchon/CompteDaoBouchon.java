package com.bankonet.dao.bouchon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bankonet.dao.CompteDAO;
import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;
import com.bankonet.model.Compte;
import com.bankonet.model.CompteCourant;
import com.bankonet.model.CompteEpargne;

/**
 * @author amalbert
 */
public class CompteDaoBouchon implements CompteDAO {

	private Map<Integer, Compte> davidMap;
	private Map<Integer, Compte> sandieMap;
	private Map<Integer, Compte> fabriceMap;

	private static Log log = LogFactory.getLog(CompteDaoBouchon.class);

	public CompteDaoBouchon() {
		davidMap = new HashMap<Integer, Compte>();
		davidMap.put(11, new CompteCourant(11, "courant david 1 bouchon", 3000, 2000));
		davidMap.put(12, new CompteCourant(12, "courant david 2 bouchon", 1000, 1000));

		sandieMap = new HashMap<Integer, Compte>();
		sandieMap.put(13, new CompteCourant(13, "courant sandie 1 bouchon", 7000, 3000));
		sandieMap.put(14, new CompteCourant(14, "courant sandie 2 bouchon", 3000, 3000));

		fabriceMap = new HashMap<Integer, Compte>();
		fabriceMap.put(15, new CompteCourant(15, "courant fabrice 1 bouchon", 1000, 4000));
		fabriceMap.put(16, new CompteCourant(16, "courant fabrice 2 bouchon", 1500, 4000));

		davidMap.put(17, new CompteEpargne(17, "epargne david 1 bouchon", 1000, 0.1f, 500));
		davidMap.put(18, new CompteEpargne(18, "epargne david 2 bouchon", 2000, 0.2f, 400));

		fabriceMap.put(19, new CompteEpargne(19, "epargne fabrice 1 bouchon", 1000, 0.1f, 500));
		fabriceMap.put(20, new CompteEpargne(20, "epargne fabrice 2 bouchon", 2000, 0.2f, 400));
	}

	public List<Compte> findComptesByClient(Client client) throws BankonetException {
		int identifiant = client.getId();
		log.info("identifiant du client : " + identifiant);

		Collection<Compte> c;
		if (identifiant == 1) {
			c = davidMap.values();
		} else if (identifiant == 2) {
			c = sandieMap.values();
		} else if (identifiant == 3) {
			c = fabriceMap.values();
		} else {
			throw new BankonetException("Compte non trouve");
		}
		return new ArrayList<Compte>(c);
	}

	public Compte findCompteByCompteId(int compteId) throws BankonetException {
		Map<Integer, Compte> globalMap = new HashMap<Integer, Compte>();
		globalMap.putAll(davidMap);
		globalMap.putAll(sandieMap);
		globalMap.putAll(fabriceMap);

		Compte compte = globalMap.get(compteId);
		if (compte != null)
			return compte;
		throw new BankonetException("Compte non trouve");
	}

	public void updateComptes(List<Compte> comptes) throws BankonetException {
		// mise � jour d�j� faite par les r�f�rences
	}

	public void transfertMontant(Compte cptSource, Compte cptDest, float montant) throws BankonetException {
		cptSource.setSolde(cptSource.getSolde() - montant);
		cptDest.setSolde(cptDest.getSolde() + montant);
	}

	public void deleteCompte(int compteId) throws BankonetException {
		Integer compteIdInteger = new Integer(compteId);

		if (davidMap.containsKey(compteIdInteger)) {
			davidMap.remove(compteIdInteger);
		} else if (sandieMap.containsKey(compteIdInteger)) {
			sandieMap.remove(compteIdInteger);
		} else if (fabriceMap.containsKey(compteIdInteger)) {
			fabriceMap.remove(compteIdInteger);
		} else {
			throw new BankonetException("Compte non trouve");
		}

	}

}