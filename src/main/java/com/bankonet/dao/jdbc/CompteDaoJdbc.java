/* Created on 26 juin 2005 */
package com.bankonet.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bankonet.dao.AbstractDAO;
import com.bankonet.dao.CompteDAO;
import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;
import com.bankonet.model.Compte;
import com.bankonet.model.CompteCourant;
import com.bankonet.model.CompteEpargne;

/**
 * @author SQLI
 */
public class CompteDaoJdbc extends AbstractDAO implements CompteDAO {

	public List<Compte> findComptesByClient(Client client) throws BankonetException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			List<Compte> compteList = new ArrayList<Compte>();
			String query = "Select coe_id, coe_description, coe_solde, coe_plafond, coe_taux, coe_decouvert_autorise,coe_discriminant FROM COMPTE WHERE clt_id=?";
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setInt(1, client.getId());

			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				if (resultSet.getString("coe_discriminant").equals("CC")) {
					CompteCourant cc = buildCompteCourant(resultSet);
					compteList.add(cc);
				} else {
					CompteEpargne ce = buildCompteEpargne(resultSet);
					compteList.add(ce);
				}
			}

			return compteList;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankonetException(e.getMessage());
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * 
	 */
	public Compte findCompteByCompteId(int compteId) throws BankonetException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			PreparedStatement statement = connection
					.prepareStatement("Select coe_id, coe_description, coe_solde, coe_plafond, coe_taux, coe_decouvert_autorise,coe_discriminant FROM COMPTE WHERE coe_id=?");
			statement.setInt(1, compteId);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			if (resultSet.getString("coe_discriminant").equals("CC")) {
				return buildCompteCourant(resultSet);
			} else {
				return buildCompteEpargne(resultSet);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankonetException(e.getMessage());
		} finally {
			closeConnection(connection);
		}
	}

	public void updateComptes(List<Compte> comptes) throws BankonetException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			connection.setAutoCommit(false);
			String query = "UPDATE COMPTE SET coe_description=?,coe_solde=? WHERE coe_id=?";
			PreparedStatement statement = connection.prepareStatement(query);

			for (Iterator<Compte> ite = comptes.iterator(); ite.hasNext();) {
				Compte c = ite.next();
				statement.setString(1, c.getLibelle());
				statement.setFloat(2, c.getSolde());
				statement.setInt(3, c.getId());
				statement.execute();
			}
			connection.commit();

		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}

			throw new BankonetException(e.getMessage());
		} finally {
			closeConnection(connection);
		}
	}

	private CompteCourant buildCompteCourant(ResultSet resultSet) throws BankonetException {
		try {
			int id = resultSet.getInt("coe_id");
			String libelle = resultSet.getString("coe_description");
			float solde = resultSet.getFloat("coe_solde");
			float decouvert = resultSet.getFloat("coe_decouvert_autorise");

			return new CompteCourant(id, libelle, solde, decouvert);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankonetException("Erreur dans le parcours du ResultSet");
		}
	}

	private CompteEpargne buildCompteEpargne(ResultSet resultSet) throws BankonetException {
		try {
			int id = resultSet.getInt("coe_id");
			String libelle = resultSet.getString("coe_description");
			float solde = resultSet.getFloat("coe_solde");
			float plafond = resultSet.getFloat("coe_plafond");
			float taux = resultSet.getFloat("coe_taux");

			return new CompteEpargne(id, libelle, solde, taux, plafond);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankonetException("Erreur dans le parcours du ResultSet");
		}
	}

	/**
	 * 
	 */
	public void transfertMontant(Compte cptSource, Compte cptDest, float montant) throws BankonetException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			boolean autoCommitDefault = connection.getAutoCommit();
			connection.setAutoCommit(false);
			String query = "UPDATE COMPTE SET coe_solde=? WHERE coe_id=?";
			PreparedStatement statement = connection.prepareStatement(query);

			// mise � jour du compte source
			statement.setFloat(1, (cptSource.getSolde().floatValue() - montant));
			statement.setInt(2, cptSource.getId());
			statement.execute();

			// mise � jour du compte destination
			statement.setFloat(1, (cptDest.getSolde().floatValue() + montant));
			statement.setInt(2, cptDest.getId());
			statement.execute();

			connection.commit();
			cptSource.setSolde(cptSource.getSolde() - montant);
			cptDest.setSolde(cptDest.getSolde() + montant);
			connection.setAutoCommit(autoCommitDefault);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankonetException(e.getMessage());
		} finally {
			closeConnection(connection);
		}
	}

	public void deleteCompte(int compteId) throws BankonetException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			boolean autoCommitDefault = connection.getAutoCommit();
			connection.setAutoCommit(false);
			PreparedStatement statement = connection.prepareStatement("DELETE FROM COMPTE WHERE coe_id=?");
			statement.setInt(1, compteId);
			statement.execute();
			connection.commit();

			connection.setAutoCommit(autoCommitDefault);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankonetException(e.getMessage());
		} finally {
			closeConnection(connection);
		}
	}

}