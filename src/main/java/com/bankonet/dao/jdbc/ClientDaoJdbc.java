package com.bankonet.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bankonet.dao.AbstractDAO;
import com.bankonet.dao.ClientDAO;
import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;

/**
 * @author amalbert
 */
public class ClientDaoJdbc extends AbstractDAO implements ClientDAO {

	public Client findClient(String p_clt_login, String p_motDePasse) throws BankonetException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			String query = "SELECT clt_id, clt_nom, clt_prenom FROM CLIENT WHERE clt_login=? and clt_mot_de_passe=?  LIMIT 1";
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setString(1, p_clt_login);
			statement.setString(2, p_motDePasse);
			ResultSet result = statement.executeQuery();

			boolean hasNext = result.next();
			if (hasNext == false) {
                throw new BankonetException("Client non trouve : clt_login ou mot de passe invalclt_ide");
			} else {
				int i = result.getInt("clt_id");
				String clt_nom = result.getString("clt_nom");
				String clt_prenom = result.getString("clt_prenom");
				Client client = new Client(i, clt_nom, clt_prenom);
				return client;

			}
		} catch (SQLException e) {
			throw new BankonetException(e);
		} finally {
			closeConnection(connection);
		}

	}

	public void updateClient(Client client) throws BankonetException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			String query = "UPDATE CLIENT SET clt_nom=?,clt_prenom=? WHERE clt_id=?";
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setString(1, client.getNom());
			statement.setString(2, client.getPrenom());
			statement.setInt(3, client.getId());
			statement.execute();

		} catch (Exception e) {
			throw new BankonetException(e);
		} finally {
			closeConnection(connection);
		}
	}

}