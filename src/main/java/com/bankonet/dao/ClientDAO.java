/*
 * Created on 13 mars 2006
 */
package com.bankonet.dao;

import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;

public interface ClientDAO {

	/**
	 * Instancie et fabrique un objet de type Client
	 */
	public abstract Client findClient(String p_login, String p_motDePasse)
			throws BankonetException;

	public abstract void updateClient(Client client) throws BankonetException;

}