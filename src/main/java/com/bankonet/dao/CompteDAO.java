/*
 * Created on 13 mars 2006
 */
package com.bankonet.dao;

import java.util.List;

import com.bankonet.model.BankonetException;
import com.bankonet.model.Client;
import com.bankonet.model.Compte;

public interface CompteDAO {

	public List<Compte> findComptesByClient(Client client) throws BankonetException;

	public Compte findCompteByCompteId(int compteId) throws BankonetException;

	public void updateComptes(List<Compte> comptes) throws BankonetException;

	public void transfertMontant(Compte cptSource, Compte cptDest, float montant) throws BankonetException;

	public void deleteCompte(int compteId) throws BankonetException;

}