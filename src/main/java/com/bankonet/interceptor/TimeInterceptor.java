package com.bankonet.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

/**
 * Created by edenayrolles on 04/12/2015.
 */
public class TimeInterceptor implements Interceptor {

    public void destroy() {
        System.out.println(this + " : destroy()");

    }

    public void init() {
        System.out.println(this + " : destroy()");
    }

    public String intercept(ActionInvocation invocation) throws Exception {

        // En amont
        long begin = System.currentTimeMillis();

        // Invocation
        String result = invocation.invoke();

        // En aval
        long end = System.currentTimeMillis();

        System.out.println("Execution time : " + (end - begin) + "ms");

        return result;
    }
}
